<?php ob_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
SESSION_START();
if($_SESSION['g'])
{
	$a=$_SESSION['g'];
}
else
{ header("location:admin.php");
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="default.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
<div id="wrapper">
	<div id="header-wrapper">
		<div id="header" class="container">
			<div id="logo">
				<h1><a href="#">Adventure Redefined</a></h1>
				<p>Explore the Unexplored</p>
			</div>
		</div>
		<div id="menu" class="container">
			<ul>
				<li><a href="index.php" accesskey="1" title="">Home</a></li>
				<li><a href="about.php" accesskey="2" title="">About Us</a></li>
				<li><a href="list.php" accesskey="3" title="">List</a></li>
				<li ><a href="top.php">Top 10</a></li>
				<li><a href="contact.php" accesskey="4" title="">Contact Us</a></li>
                 <li><a href="video.php" accesskey="5" title="">Videos</a></li>
                 <li class="current_page_item"><a href="admin.php" accesskey="6" title="">Admin</a></li> 
			</ul>
			</ul>
		</div>
	</div>
	<div id="page" class="container">
		
			<p><strong>Update list:</strong><br /><br />
<form action="" method="post" enctype="multipart/form-data">
State<input type="text" name="s" />
Title<input type="text" name="t" />
Content<input type="text" name="c" />
Image<input type="file" name="up" />
<input type="submit" name="n" value="submit" />
</form>
<?php
include("db.php");
if(isset($_POST['n']))
{   $name=$_POST['s'];
	$title=$_POST['t'];
	$content=$_POST['c'];
	$uploaddir='file_upload/';
				$uploadfile=$uploaddir.basename($_FILES['up']['name']);
				if(move_uploaded_file($_FILES['up']['tmp_name'],$uploadfile))
				{
				   $query="insert into list values('','$name','$title','$content','$uploadfile')";
				   mysql_query($query);
				}
}
?>
<br /><br /> <br /><br />

<strong>Update Top 10:</strong><br /><br />



<form action="" method="post" enctype="multipart/form-data">
Title<input type="text" name="t" />
Name<input type="text" name="n" />
Image<input type="file" name="up" />
Content<textarea  name="c" ></textarea>
<input type="submit" name="bt" value="submit" />
</form>
<?php
include("db.php");
if(isset($_POST['bt']))
{   
	$title=$_POST['t'];
	$name=$_POST['n'];
	$content=$_POST['c'];
	$uploaddir='file_upload/';
				$uploadfile=$uploaddir.basename($_FILES['up']['name']);
				if(move_uploaded_file($_FILES['up']['tmp_name'],$uploadfile))
				{
				   $query="insert into top values('','$title','$name','$uploadfile','$content')";
				   mysql_query($query);
				}
}
?>
<br /><br />
<form action="" method="post">
<input type="submit" name="lg" value="Logout" />
</form>
<?php
if(isset($_POST['lg']))
{  
  SESSION_DESTROY();
  header("location:index.php");
}
?>

            </p>
		</div>
			<div id="portfolio-wrapper">
		<div id="portfolio" class="container">
			<div class="title">
				<h2>Adventure Redefined</h2>
				<span class="byline">Explore the unexplored!!</span> </div>
			<div class="column1">
				<div class="box">
					<h3>About us</h3>
					<p>we are providing you India's most daring and fun adventures.</p>
					<a href="about.php" class="button">Read More</a> </div>
			</div>
			<div class="column2">
				<div class="box">
					<h3>List</h3>
					<p>Click to view the list of adventures with complete information.</p>
					<a href="list.php" class="button">Explore list</a> </div>
			</div>
			<div class="column3">
				<div class="box">
					<h3>TOP 10</h3>
					<p>View best 10 locations for adventure sports of all type in india.</p>
					<a href="top.php" class="button">View top10</a> </div>
			</div>
			<div class="column4">
				<div class="box">
					<h3>Contact us</h3>
					<p> click here to associate and get in touch with us.</p>
					<a href="contact.php" class="button">Contact</a> </div>
			</div>
		</div>
	</div>
</div>
<div id="footer">
	<p>&copy; 2015. All rights reserved. Design by Surabhi shekhawat</p>
</div>
</body>
</html>
