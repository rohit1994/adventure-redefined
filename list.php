<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="default.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
<div id="wrapper">
	<div id="header-wrapper">
		<div id="header" class="container">
			<div id="logo">
				<h1><a href="#">Adventure Redefined</a></h1>
				<p>Explore the Unexplored</p>
			</div>
		</div>
		<div id="menu" class="container">
			<ul>
				<li><a href="index.php" accesskey="1" title="">Home</a></li>
				<li><a href="about.php" accesskey="2" title="">About Us</a></li>
				<li class="current_page_item"><a href="list.php">List</a></li>
				<li><a href="top.php" accesskey="3" title="">Top 10</a></li>
				<li><a href="contact.php" accesskey="4" title="">Contact Us</a></li>
                 <li><a href="video.php" accesskey="5" title="">Videos</a></li> 
                 <li><a href="admin.php" accesskey="6" title="">Admin</a></li>
			</ul>
			</ul>
		</div>
	</div>
	<div id="page1" > 
    <h1>Adventure List</h1></br></br>
			<p>
<strong><font size="+3">Arunachal Pradesh</font></strong><br/>
1.Biking Tours<br/>If you want to have a tryst with the mountains,rugged & challenging terrains, extreme passes or even hanging bridges, Arunachal Pradesh should be your next destination. Also known as the hidden land, this state boasts of being a paradise for the biking enthusiasts!<br/><br/>

<img src="images/1.jpg"/><br/>
2.Siang Valley Trekking<br/>
An exhilarating trek of 11 days in this mystique valley of Arunachal Pradesh is all about exploring and discovering the opulent nature of the state. Considered as a moderate trek, it is an affair of lush greeneries, narrow and enthralling passes, awe-inspiring terrains, challenging trails and lot more.<br/><br/>
<img src="images/2.jpg"/><br/>

3.Angling in Subansiri<br/>
Originating from the mighty Himalayas in Tibet, the Subansiri River flowing through Arunachal Pradesh is amongst the most sought after destination for Golden Masheer, Trout and Goonj fishing sports. If you are an angling fan, this river and its fascinating dwellers will offer you the ultimate thrill of angling!<br/><br/>

4.White Water Rafting in Subansiri<br/>
Visit Arunachal Pradesh, a hidden gem in the North-Eastern part of India, carve out the fear in you and challenge the white waters of Subansiri. Feel the thrill of manoeuvring the gushing the rapids of Grade-I to Grade-IV and curate some of the spine-chilling adventurous moments of your life.<br/><br/>
<img src="images/4.jpg"/><br/><br/>

<strong><font size="+3">Assam</font></strong><br/>
 
 5.One-Horned Rhino Spotting in Kaziranga National Park<br/>
 While in the Kaziranga National Park in Assam, take an enthralling Elephant Safari and step into the enchanting world of the One-horned Rhinos! In addition to this exotic as well as endangered species, you can also spot other majestic wildlife like the Indian Tigers and Wild Buffaloes.<br/><br/>
 <img src="images/5.jpg"/><br/>

 6.River Rafting and Angling in Jia Bhorali, Assam<br/>
Does challenging the arduous rapids give you the required adrenaline kick? If yes, then head to the Nameri National Park in Assam and indulge in river rafting in the Brahmaputra River.<br/><br/>
<img src="images/6.jpg"/><br/>

7.Wildlife Safari in Nameri National Park<br/>
This national wildlife protected area is the home to several endangered and exotic wildlife species. To name a few, you can easily spot Tiger, Elephant, Leopard, Wild Boar, Gaur, Himalayan Black Bear and others. Housing more 300 species of birds and around 600 types of flora, it is also a paradise for nature lovers and shutterbugs.<br/><br/>
<img src="images/7.jpg" /><br/>

<strong><font size="+3"> Bihar</font></strong><br/>
8.Bungee Jumping in Jagdalpur, Chhattisgarh<br/>
What can be the better way to overcome the fear of height than to go for a bungee jump in the Jagdalpur region of Chhattisgarh? Fall freely from several hundred feet; feel and enjoy the sudden adrenaline rush in you!<br/><br/>
 <img src="images/8.jpg" /><br/>
 
<strong><font size="+3">Goa</font></strong><br/>
9.Snorkeling and Scuba Diving<br/>
Be it over or under the waters, Goa never ceases to amaze its visitors. On your next trip to this beach capital, make sure that you dive into the turquoise waters and explore the divine beauty of Goa’s marine life and quench you adventure lust!<br/><br/>
<img src="images/9.jpg" /><br/>

10.Paragliding in Goa<br/>
Give wings to your dream of flying, witness the magical beauty of the tepid beaches and azure waters like a bird! Goa, with all its glory, proves to be an ideal destination for paragliding and other adventure sports.<br/><br/>
<img src="images/10.jpg" /><br/>

11.Underwater Walk<br/>
A mile’s walk a day, keeps the doctor away…and if the walking track is under the groovy waters of Goa, you have all the reasons to enjoy your walk.Go Goa and you can get the opportunity to walk amongst the stunning marine life and enjoy its celestial beauty!<br/><br/>
<img src="images/11.jpg" /><br/>

12.Dudhsagar Trek<br/>
Not just a spectacular waterfall, but also an important landmark of Goa, the Dudhsagar Waterfall is known for its mesmeric beauty and appeal. It is in this vicinity, where you can get indulged into an enthralling trek that will lead you to the base of the mystique cascade.<br/><br/>
<img src="images/12.jpg" /><br/>

13.Surfing<br/>
The moment you realise, the azure waters excite you or offers you the required adrenaline kick, you know where to go! Surfing in Goa is all about fun, adventure and romancing the waves of the Arabian Sea.<br/><br/>
<img src="images/13.jpg" /><br/>

14.Overnight Trekking in Tambdi Surla<br/>
If you still think Goa is all about parties and beaches, you should make it to Tambdi Surla. An enthralling overnight trekking destination, this sojourn will definitely satiate all your adventure yearnings.<br/><br/>

<strong><font size="+3">Gujarat</font></strong><br/>
 15.Motorcycle Trip to Rann of Kutch<br/>
 How about a motorcycle trip to nowhere? Yes, you heard right…a trip to nowhere! Kick stasrt your engine and gear up for an amazing ride to the Great Rann of Kutch in Gujarat, and have a rendezvous with the nothingness of this salt desert!<br/><br/>
 <img src="images/15.jpg" /><br/>
 
 <strong><font size="+3">Himachal Pradesh</font></strong><br/>
 16.Cycling in the Kangra Valley<br/>
 Challenge and push your own limit with a cycling trip in the Kagnra Valley of Himachal Pradesh. Pedalling 260km at a height of 4,940ft can easily get you high on adrenalines!<br/><br/>
 <img src="images/16.jpg" /><br/>

17.Deotibba Hampta Pass<br/>
A moderate trek that can be taken throughout the year, this trek takes you through a fascinating range of trails. While in some places it passes through barren lands, lush forests, at other points, it will take you through scenic meadows, windy trails and snow clad peaks.<br/><br/>
<img src="images/17.jpg" /><br/>

18.Skiing in Kufri<br/>
Whether you are a beginner or a pro, skiing in Himachal Pradesh will surely make you fall in love with the snows! Among all the skiing destinations, Mahasu and Narkanda are the two most popular skiing destinations in the state.<br/><br/>
<img src="images/18.jpg" /><br/>

19.White Water Rafting in Kullu<br/>
The Beas River in Kullu Valley is amongst the most sought after white water rafting destinations in the country. High grade rapids, exotic views all around and the thrill of challenging the gushing waves; Kullu Valley has all that it takes for an enthralling white water rafting expedition.<br/><br/>
<img src="images/19.jpg" /><br/>

20.Heli skiing at Hanuman Tibba<br/>
The fun and thrill of heli-skiing begins with the drop at high altitudes and continues till you reach the skiing base! The powdery white snow blanket that cossets Hanuman Tibba is one such destination, where heli-skiing can be enjoyed at the best.<br/><br/>
<img src="images/20.jpg" /><br />

21.Zorbing and Paragliding in Manali<br />
While in Manali, you can enjoy its prismatic beauty in many different ways. Be it like a bird or sliding down the charismatic slopes, this quaint tourist destination in Himachal Pradesh is all about fun and adventure. Zorbing and Paragliding are two of the most trending adventure activities among the adventure junkies in Manali.<br /><br />
<img src="images/21.jpg" /><br />

 <strong><font size="+3">Jammu and Kashmir</font></strong><br />
 22.Parang La Trek<br />
Lasting for around 9 days and originating from Kibber, this one is amongst the most challenging treks in India. While challenging the trails of this exhilarating trek, you can also have the glimpses of the Indo-Tibetan Border and if you are lucky enough, you can also spot the charismatic snow leopards.<br /><br />
<img src="images/22.jpg" /><br />

23.Heli Skiing in Gulmarg<br />
Though heli-skiing is amongst the newest discovery in Gulmarg, Jammu and Kashmir, it is gaining extreme popularity amidst the adrenaline junkies. A chopper ride to those areas that cannot be easily reached and skiing down to the base has its own delight, fun and excitement level.<br /><br />
    <img src="images/23.jpg" /><br />
    
 <strong><font size="+3">Karnataka</font></strong><br />
 24.Kudremukh Trek<br />
Chikmagalur, with all its natural marvels, boasts of being an opulent destination for the adventure junkies. The Kudremukh Peak, one of the main attractions of Chikmagalur entices the trekkers with its easy to difficult trails. Resembling the face of a horse, this peak is one of the highest trekked peaks in Karnataka.<br /><br />
   <img src="images/24.jpg" /><br />
   
25.Microlight Flying in Bangalore<br />
Give a new definition to fun and adventure with a microlight flight in the Jakkur Airfield in Bangalore! Fly over the scenic landscapes of the Garden City with a trained pilot in a powered hang glider and enjoy every bit of it.<br /><br />
<img src="images/25.jpg" /><br />

<strong><font size="+3">Kerala</font></strong><br />
26.Para Sailing at Payyambalam beach<br />
How about an aerial view of the scenic Payyambalam Beach and the turquoise waters of the Arabian Sea? Sounds exciting, right? Visit this stunning beach in God’s Own Country and get indulged in a parasailing ride, fly up to 300ft and enjoy the bird’s eye view of the beach and the sea.<br /><br />
<img src="images/26.jpg" /><br />

27.Bamboo Rafting in Periyar National Park<br />
If you want to witness the fascinating wildlife of Periyar National Park in their most natural habitats, you must go for bamboo rafting in the Periyar Lake. If luck favours, you can also spot some of the dwellers of the park beside the lake coming down for water and prey.<br /><br />
<img src="images/27.jpg" /><br />
   
 <strong><font size="+3">Maharashtra</font></strong><br />
28.Kayaking in Mumbai<br />
    For those, who had a fondness for the azure waters, Kayaking is the ideal way to quench all the adventure yearnings. Usually, one can participate and enjoy this activity in the Chowpatty suburb of Mumbai and ravel the allurement of the Arabian Sea.<br /><br />
<img src="images/28.jpg" /><br />

29.Camping in Lonavala, Maharashtra<br />
The exciting and appealing locales of Lonavala are indeed one of the best and ideal camping destinations in Maharashtra. Jotted with scenic hills and mountains, lush meadows and several spots for camping, one can also enjoy the trills of waterfall rappelling and ziplining.<br /><br />
<img src="images/29.jpg" /><br />

<strong><font size="+3">Rajasthan</font></strong><br />
 30.Desert Camping in Jodhpur<br />
For those, who crave for fun and adventure in the arid deserts of Rajasthan, Jodhpur proves to be an ideal destination. Also known as the ‘Sun City’, the adventure junkies can opt for desert camping along with bonfire, barbeque dinners and village tours in Jodhpur.<br /><br />
<img src="images/30.jpg" /><br />

31.Cycling Tours in Jaipur<br />
The adventure side of the ‘Pink City’ or Jaipur lies in discovering its royalty and magnificence on wheels. On a usual cycling tour of Rajasthan’s capital city, you can visit a number of major landmarks including the Jal Mahal, Nahargarh Fort and many others during the course.<br /><br />
<img src="images/31.jpg" /><br />

32.Flying Fox Adventure in Neemrana Fort, Rajasthan<br />
How about adventure mixed with the essence of royalty? Sounds exciting, isn’t it? And to experience this, all you need to do: visit the Neemrana Fort in Rajasthan and enjoy ziplining over the fort!<br /><br />
<img src="images/32.jpg" /><br />

<strong><font size="+3">Sikkim</font></strong><br />
33.Mt. Kanchenjunga Trek, Sikkim<br />
The Mt. Kanchenjunga Trek is a call for the ardent trekkers. Though the trek offers challenging trails, extreme conditions and narrow passes through towering peaks, the fascinating appeal of the Himalayan Range and its peaks allures adventure junkies from across the globe to undertake this trek.<br /><br />
<img src="images/33.jpg" /><br />

<strong><font size="+3">Uttarakhand</font></strong><br />
34.River Rafting in Rishikesh<br />
Delhi might be capital of the country, but when it comes to adventure sports and activities, Rishikesh boasts of being the capital of India. The Ganga River flowing through the city offers rapids ranging from Grade-I-V and makes it for an ultimate rafting destination in the country.<br /><br />
<img src="images/34.jpg" /><br />

35.Skiing in Auli<br />
 Come winter, the Garhwal Himalayas in Auli paint themselves with the natural white of the snows. During this season, adventure junkies from all over the world throng the Himalayas and enjoy the thrill ride with their skis. Favourable slopes, thick layer of snow and scenic views all around makes it for a memorable skiing experience in Auli.<br /><br />
 <img src="images/35.jpg" /><br />

 36.Valley Of Flowers Trek<br />
A trek that takes you to a mesmerising valley that is perched at a height of 12,000ft, this trek is one of the most sought after treks in India. With the Valley of Flowers as the final destination, this moderate trek might give you real hard times while ascending uphill.<br /><br />
<img src="images/36.jpg" /><br />

37.Har ki Dun Trek<br />
Not only the trekkers, but the nature lovers can also enjoy this enthralling trek in the Garhwal Himalayas. Originating from the Sankri region in Uttarakhand this 77km trek can usually be completed in a span of 8 days.<br /><br />
<img src="images/37.jpg" /><br />

38.Homkund Ronti Saddle<br />
 Though this trek is amongst the newest treks in the country, it has gained immense popularity with the trekkers. Taking the trekkers alongside the mystique Roopkund Lake, this moderate grade trek reaches a height of 16,8636ft and can be completed within 11 days.<br /><br />
 <img src="images/38.jpg" /><br />
 
39.Neel Kantha Base Camp<br />
Enchanting views of the Neel Kantha Peak in the early hours of the day are enough to infect the trekkers with the desire of trekking to its summit! Rising above all the surrounding peaks, the trek to this peak is one of the strenuous one in the country and is filled with surprises all the way.<br /><br />
<img src="images/39.jpg" /><br />

 40.Rupin Pass Trek<br />
 Walking along the edges of the fascinating Garhwal Himalayas is itself one of the most enthralling things to do in Uttarakhand! Passing through several natural marvels, the Rupin Pass trek; conducted during May-June and September-October, boasts of being a moderate to strenuous trek in the Himalayas.<br /><br />
 <img src="images/40.jpg" /><br />
 
41.Milam Glacier Trek<br />
Like the Garhwal Himalayas, the Kumaon region is also the origin of several exhilarating treks. Lasting for around 13-15 days, the Milam Glacier Trek is one of the strenuous treks that can provide the most sought after adrenaline kicks to the trekkers during the months of June-October.<br /><br />
<img src="images/41.jpg" /><br />

42.Biking, Hiking and Rafting in Mandakini and Alaknanda Valley<br />
Be it pedalling through the invigorating terrains, or hiking through the steep slopes and challenging passes, or challenging the splashing waves, this tour outlooks most of the adventure packages! Moreover, on this expedition, the participants get to visit Rishikesh, Devprayag, Deorital Lake, Tungnath Temple and other scenic delights in Uttarakhand.<br /><br />
<img src="images/42.jpg" /><br />

43.Jim Corbett Safari<br />
Whether wildlife spotting or a safari through the thickets, Jim Corbett National Park has its own charm and splendours! Home to some of the most exotic wildlife, the wildlife adventure junkies can go for either a Jeep or Elephant safari in the park and discover the enthralment of the park.<br /><br />

<strong><font size="+3">Tamil nadu</font></strong><br />
44.Crocodile Bank Visit in Mahabalipuram<br />
How about witnessing the cold blooded reptiles in their natural habitats? Even if this one does not sound enthralling, how about feeding them? Yes, on a visit to the Crocodile Bank in Mahabalipuram, you can have an adventurous encounter with the crocodiles and have a chance to feed them. While visiting this crocodile breeding farm, you can also visit the Snake Breeding Centre and witness some of the most venomous snakes in India.<br /><br />
<img src="images/44.jpg" /><br />

 <strong><font size="+3"> West Bengal</font></strong><br />
 45.Sandakphu Trekking<br />
Standing graciously at 11,941ft and along the edge of the Singalila National Park, Sandakphu is the highest point of West Bengal. A trek to this point is all about mesmerising views of the Himalayan Ranges, passing through enchanting trails and getting the right adrenaline dose!<br /><br />
<img src="images/45.jpg" /><br />

46.White Water Rafting in Teesta River<br />
 Darjeeling, a pleasant town at the foothills of the Himalayas, where the Teesta River brings in a new definition of fun and adventure, is gaining immense popularity as a white water rafting destination. Flowing through the hills and plains, the Teesta River offers several rapids that entice attract from across the country.<br /><br />
 <img src="images/46.jpg" /><br />
 
 47.Tiger Spotting in the Sunderbans<br />
Situated on the Deltaic region of West Bengal, Sunderbans is the indigenous home to the Royal Bengal Tigers. Tiger spotting in this national park is a unique and unmatched experience in the whole world and is a must for all the wildlife lovers.<br /><br />
<img src="images/47.jpg" /><br />

<strong> <font size="+3">Andaman and Nicobar Islands</font></strong><br />
48.Scuba Diving<br />
 In addition to the surreal beauty of the landscapes, groovy beaches and the lush greeneries, Andaman also takes the pride of being the home to a prismatic marine life. And to discover the magical beauty of the under-water life, scuba diving in Andaman’s Elephant Beach and several other spots proves to be most enthralling as well as adventurous way of getting introduced with its diverse marine life.<br /><br />
 <img src="images/48.jpg" /><br />
 
49.Snorkelling Andamans<br />
 Within this tour that lasts for around 1-1.5 hours, you will surely be astounded with the kaleidoscopic beauty of Andaman’s stunning marine life. Ideal for almost everyone, the fun, excitement and adventure related to snorkelling in unlimited and is an experience of its own kind!<br /><br />

 <strong><font size="+3">Chandigarh</font></strong><br />
 50.Flying Fox in Kikar<br />
 Embraced by the Shivalik Range, Kikar is an adventurous getaway in Chandigarh. Its untouched beauty and favourable locales make it a befitting destination for adventure activities like flying fox. A 5 zip-line tour that lasts for around 2-2.5 hours, this adrenaline pumping activity allows you to enjoy the aerial views of Kikar’s pristine beauty.<br /><br />
<img src="images/50.jpg" /><br />
</p>

<?php 
include("db.php");
$q=mysql_query("select * from list");
while($qq=mysql_fetch_array($q))
{
	?>

 <strong><font size="+3"><?php echo $qq['state']; ?></font></strong><br />
 <?php echo $qq['title']; ?><br />
 <?php echo $qq['content']; ?><br /><br />
<img src="<?php echo $qq['upd']; ?>" /><br />
</p>
<?php } ?>

</div>
	<div id="portfolio-wrapper">
		<div id="portfolio" class="container">
			<div class="title">
				<h2>Adventure Redefined</h2>
				<span class="byline">Explore the unexplored!!</span> </div>
			<div class="column1">
				<div class="box">
					<h3>About us</h3>
					<p>we are providing you india's most daring and fun adventures.</p>
					<a href="about.php" class="button">Read More</a> </div>
			</div>
			<div class="column2">
				<div class="box">
					<h3>List</h3>
					<p>Click to view the list of adventures with complete information.</p>
					<a href="list.php" class="button">Explore list</a> </div>
			</div>
			<div class="column3">
				<div class="box">
					<h3>TOP 10</h3>
					<p>View best 10 locations for adventure sports of all type in india.</p>
					<a href="top.php" class="button">View top10</a> </div>
			</div>
			<div class="column4">
				<div class="box">
					<h3>Contact us</h3>
					<p> click here to associate and get in touch with us.</p>
					<a href="contact.php" class="button">Contact</a> </div>
			</div>
		</div>
	</div>
</div>
<div id="footer">
	<p>&copy; 2015. All rights reserved. Design by Surabhi shekhawat</p>
</div>
</body>
</html>
