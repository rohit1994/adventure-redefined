<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="default.css" rel="stylesheet" type="text/css" media="all" />
<!-- Start Slider HEAD section -->
<link rel="stylesheet" type="text/css" href="engine1/style.css" />
<script type="text/javascript" src="engine1/jquery.js"></script>
<!-- End Slider HEAD section -->
</head>
<body>
<div id="wrapper">
	<div id="header-wrapper">
		<div id="header" class="container">
			<div id="logo">
				<h1><a href="#">Adventure Redefined</a></h1>
				<p>Explore the Unexplored</p>
			</div>
		</div>
		<div id="menu" class="container">
			<ul>
				<li><a href="index.php" accesskey="1" title="">Home</a></li>
				<li class="current_page_item"><a href="about.php">About Us</a></li>
				<li><a href="list.php" accesskey="2" title="">List</a></li>
				<li><a href="top.php" accesskey="3" title="">Top 10</a></li>
				<li><a href="contact.php" accesskey="4" title="">Contact Us</a></li>
                 <li><a href="video.php" accesskey="5" title="">Videos</a></li> 
                 <li><a href="admin.php" accesskey="6" title="">Admin</a></li>
			</ul>
			</ul>
		</div>
	</div>
	<div id="page" class="container">
	<div style="width:500px; float:left;">
    <h3>About Us:</h3></br></br>	
			<p>If you are an adventurous kind of a person and looking out to spend your leisure time in some adventurous sports, then <strong>ADVENTURE REDEFINED</strong> is surely a destination for you. From trekking to mountain biking, from yachting to river rafting there is lot of adventurous sports that one can indulge in.<br/><br/>

Enjoy the experience of trekking through the lush green forests or indulge in mountain climbing, opt for mountain biking along the unknown path, or opt for rock climbing along the mountainous hills, or opt for canoeing, or opt for yachting, river rafting and long-distance swimming. Whatever your choice may be, there is complete information about all you want to know.</p>
		</div> 
        
<div style="float:right; width:600px;">        
        
            <!-- Start Slider BODY section -->
<div id="wowslider-container1">
<div class="ws_images"><ul>
		<li><img src="data1/images/2.jpg" alt="2" title="2" id="wows1_0"/></li>
		<li><img src="data1/images/4.jpg" alt="4" title="4" id="wows1_1"/></li>
		<li><img src="data1/images/5.jpg" alt="5" title="5" id="wows1_2"/></li>
		<li><img src="data1/images/6.jpg" alt="6" title="6" id="wows1_3"/></li>
		<li><img src="data1/images/7.jpg" alt="7" title="7" id="wows1_4"/></li>
		<li><img src="data1/images/8.jpg" alt="8" title="8" id="wows1_5"/></li>
		<li><img src="data1/images/9.jpg" alt="9" title="9" id="wows1_6"/></li>
		<li><img src="data1/images/10.jpg" alt="10" title="10" id="wows1_7"/></li>
		<li><img src="data1/images/12.jpg" alt="12" title="12" id="wows1_8"/></li>
		<li><img src="data1/images/13.jpg" alt="13" title="13" id="wows1_9"/></li>
		<li><img src="data1/images/15.jpg" alt="15" title="15" id="wows1_10"/></li>
		<li><img src="data1/images/16.jpg" alt="16" title="16" id="wows1_11"/></li>
		<li><a href="http://wowslider.com"><img src="data1/images/17.jpg" alt="video slider" title="17" id="wows1_12"/></a></li>
		<li><img src="data1/images/18.jpg" alt="18" title="18" id="wows1_13"/></li>
	</ul></div>
<div class="ws_script" style="position:absolute;left:-99%"><!--<a href="http://wowslider.com">slider bootstrap</a> by WOWSlider.com v8.2--></div>
<div class="ws_shadow"></div>
</div>	
<script type="text/javascript" src="engine1/wowslider.js"></script>
<script type="text/javascript" src="engine1/script.js"></script>
<!-- End Slider BODY section -->

        </div>
        
        </div>
			<div id="portfolio-wrapper">
		<div id="portfolio" class="container">
			<div class="title">
				<h2>Adventure Redefined</h2>
				<span class="byline">Explore the unexplored!!</span> </div>
			<div class="column1">
				<div class="box">
					<h3>About us</h3>
					<p>we are providing you india's most daring and fun adventures.</p>
					<a href="about.php" class="button">Read More</a> </div>
			</div>
			<div class="column2">
				<div class="box">
					<h3>List</h3>
					<p>Click to view the list of adventures with complete information.</p>
					<a href="list.php" class="button">Explore list</a> </div>
			</div>
			<div class="column3">
				<div class="box">
					<h3>TOP 10</h3>
					<p>View best 10 locations for adventure sports of all type in india.</p>
					<a href="top.php" class="button">View top10</a> </div>
			</div>
			<div class="column4">
				<div class="box">
					<h3>Contact us</h3>
					<p> click here to associate and get in touch with us.</p>
					<a href="contact.php" class="button">Contact</a> </div>
			</div>
		</div>
	</div>
</div>
<div id="footer">
	<p>&copy; 2015. All rights reserved. Design by Surabhi shekhawat</p>
</div>
</body>
</html>
