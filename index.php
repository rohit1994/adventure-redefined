<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="default.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="images/adventure_sports.jpg" />
</head>
<body>
<div id="wrapper">
	<div id="header-wrapper">
     <div id="header" class="container">
         <div id="logo">
				<h1><a href="#">Adventure Redefined</a></h1>
				<p>Explore the Unexplored</p>
			</div>
		</div>
		<div id="menu" class="container">
			<ul>
				<li class="current_page_item"><a href="#">Home</a></li>
				<li><a href="about.php" accesskey="1" title="">About Us</a></li>
				<li><a href="list.php" accesskey="2" title="">List</a></li>
				<li><a href="top.php" accesskey="3" title="">Top 10</a></li>
				<li><a href="contact.php" accesskey="4" title="">Contact Us</a></li>
                <li><a href="video.php" accesskey="5" title="">Videos</a></li> 
                <li><a href="admin.php" accesskey="6" title="">Admin</a></li>
			</ul>
		</div>
	</div>
	<div id="page" class="container">
		<div class="column1">
			<div class="title">
				<h2>Adventures!!</h2>
			</div>
			<p>Bestowed with towering peaks and mountains, fascinating rivers, challenging terrains and other natural marvels,<strong>India</strong> has become an epitome of adventure sports in the world.<br /> <br/>
Here is a list of adventure activities to do in India that will guide you through your adventure quest. Make sure, you don’t get spoilt with abundance of choices for your next adventure tour! Have fun :) </p>
		</div>
		<div class="column2">
			<div class="title">
				<h2>Dream!!</h2>
			</div>
			<img src="images/pic01.jpg" width="282" height="150" alt="" />
			<p>Tired of feeling stuck? Let go of the past and create a life you love-a life full of adventures!</p>
		</div>
		<div class="column3">
			<div class="title">
				<h2>Discover!!</h2>
			</div>
			<img src="images/pic02.jpg" width="282" height="150" alt="" />
			<p>It’s really easy to get caught up in a new adventure and discover the beauty of nature and your inner self! </p>
		</div>
		<div class="column4">
			<div class="title">
				<h2>Dare!!</h2>
			</div>
			<img src="images/pic03.jpg" width="282" height="150" alt="" />
			<p>Life is inherently risky.There is only one big risk you should avoid at all costs,and that is the risk of doing nothing!</p>
		</div>
	</div>
	<div id="portfolio-wrapper">
		<div id="portfolio" class="container">
			<div class="title">
				<h2>Adventure Redefined</h2>
				<span class="byline">Explore the unexplored!!</span> </div>
			<div class="column1">
				<div class="box">
					<h3>About us</h3>
					<p>We are providing you india's most daring and fun adventures.</p>
					<a href="about.php" class="button">Read More</a> </div>
			</div>
			<div class="column2">
				<div class="box">
					<h3>List</h3>
					<p>Click to view the list of adventures with complete information.</p>
					<a href="list.php" class="button">Explore list</a> </div>
			</div>
			<div class="column3">
				<div class="box">
					<h3>TOP 10</h3>
					<p>View best 10 locations for adventure sports of various types in india.</p>
					<a href="top.php" class="button">View top10</a> </div>
			</div>
			<div class="column4">
				<div class="box">
					<h3>Contact us</h3>
					<p> Click here to associate and get in touch with us.</p>
					<a href="contact.php" class="button">Contact</a> </div>
			</div>
		</div>
	</div>
</div>
<div id="footer">
	<p>&copy; 2015. All rights reserved. Design by Surabhi shekhawat</p>
</div>
</body>
</html>